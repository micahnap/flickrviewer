//
//  ViewController.m
//  FlickChallenge
//
//  Created by Micah on 12/11/2014.
//  Copyright (c) 2014 Micah. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "DisplayCell.h"
#import "ThumbNailCell.h"
#import "FlickrPhotos.h"

#define FlickrURL @"https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

- (IBAction)btnRefreshPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewThumbnail;
@property (strong, nonatomic) NSArray *flickrDataArray;
@property (strong, nonatomic) NSArray *animations;
@property CGPoint scrollPositionBeforeRotation;

@end

@implementation ViewController

#pragma mark - Overides
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.animations = [self arrayForAnimations];
    [self loadArrayFromFlickrPhotos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Utilities
//returns an array of images for animating loading image
-(NSArray *)arrayForAnimations
{
    NSMutableArray *animationArray = [NSMutableArray new];
    
    for (int i = 0; i <= 10; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading%i.png",i]];
        [animationArray addObject:image];
    }
    return [animationArray copy];
}

//fetch jSON data from Flickr API
-(void)loadArrayFromFlickrPhotos
{
    [self.activityIndicator startAnimating];
    NSMutableArray *photoArray = [NSMutableArray new];
    
    //get image data from web call
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //Flickr service returns json data in web form so AFHTTPResponseSerializer is used to parse
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:FlickrURL
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSError *error;
             NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             //the web service annoyingly returns invalid JSON on occasion. Replace invalid characters with valid ones in the event it happens.
             NSString *cleanString = [responseString stringByReplacingOccurrencesOfString: @"\\'" withString: @"'"];
             NSData *jsonData = [cleanString dataUsingEncoding:NSUTF8StringEncoding];
             NSDictionary *parentDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
             NSArray *jsonArray = parentDictionary[@"items"];
             
             //create FlickrPhoto objects from json object. selects first image as default
             [jsonArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
              {
                  NSDictionary *mediaDict = obj;
                  NSDictionary *imageURLDict = mediaDict[@"media"];
                  NSString *imageURL = imageURLDict[@"m"];
                  
                  FlickrPhotos *flikrPhoto = [[FlickrPhotos alloc] initWithImageURL:imageURL andSelection:idx == 0 ? true : false];
                  [photoArray addObject:flikrPhoto];
              }];
             
             //reset content offset for refreshing
             self.flickrDataArray = [photoArray copy];
             [self.activityIndicator stopAnimating];
             [self.collectionView reloadData];
             [self.collectionViewThumbnail reloadData];
             
             //for refreshing. sets collection views back to the beginning
             self.collectionView.contentOffset = CGPointMake(0,0);
             self.collectionViewThumbnail.contentOffset = CGPointMake(0,0);
             
         }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error retrieving data" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             
             [alert show];
         }];
}

//downloads image asynchonously, passes back to cell image view via completion block
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

#pragma mark - Orientation
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //transition to landscape looks a bit funny due to content offset reset, hide view to make it smoother
    self.collectionView.alpha = 0;
    
    //update flow layout
    [self.collectionView.collectionViewLayout invalidateLayout];
    
    //retains current position of collection view so user is viewing same image on rotation
    self.scrollPositionBeforeRotation = CGPointMake(self.collectionView.contentOffset.x / self.collectionView.contentSize.width,
                                                    self.collectionView.contentOffset.y / self.collectionView.contentSize.height);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
{
    //set collection view to proper place and make visible
    CGPoint newContentOffset = CGPointMake(self.scrollPositionBeforeRotation.x * self.collectionView.contentSize.width,
                                           self.scrollPositionBeforeRotation.y * self.collectionView.contentSize.height);
    
    [self.collectionView setContentOffset:newContentOffset animated:NO];
    
    self.collectionView.alpha = 1;
}

#pragma mark - UICollectionViewDelegate/DataSource
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //select cell user tapped, scroll big image view to selected image
    if (collectionView == self.collectionViewThumbnail)
    {
        FlickrPhotos *photo = self.flickrDataArray[indexPath.row];
        
        //deselect the selected apart from the user's  current choice
        for (FlickrPhotos *flickPhoto in self.flickrDataArray)
        {
            flickPhoto.selected = [flickPhoto isEqual:photo] ? true : false;
        }
        
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        
        [self.collectionViewThumbnail reloadData];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize retval = collectionView == self.collectionView ? CGSizeMake(self.view.bounds.size.width, self.collectionView.bounds.size.height) : CGSizeMake(110, 110);
    return retval;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.flickrDataArray ? [self.flickrDataArray count] : 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //presentation management for two collection views
    FlickrPhotos *flickrPhoto = self.flickrDataArray[indexPath.row];
    
    if (cv == self.collectionView)
    {
        DisplayCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"FlickrCell" forIndexPath:indexPath];
        if (flickrPhoto.thumbnail)
        {
            cell.imageView.image = flickrPhoto.thumbnail;
        }
        else
        {
            // set default user image while image is being downloaded
            cell.imageView.animationImages = self.animations;
            [cell.imageView startAnimating];
            // download the image asynchronously
            [self downloadImageWithURL:[NSURL URLWithString:flickrPhoto.photoURL] completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded)
                {
                    [cell.imageView stopAnimating];
                    // change the image in the cell
                    cell.imageView.image = image;
                    
                    // cache the image for use later (when scrolling up)
                    flickrPhoto.thumbnail = image;
                }
            }];
        }
        return cell;
    }
    
    else
    {
        ThumbNailCell *tbCell = [cv dequeueReusableCellWithReuseIdentifier:@"FlickrCellThumb" forIndexPath:indexPath];
        
        //load cached image into cell if applicable
        if (flickrPhoto.thumbnail)
        {
            tbCell.imageView.image = flickrPhoto.thumbnail;
        }
        
        else
        {
            //add loading animation to cells that are downloading images
            tbCell.imageView.animationImages = self.animations;
            [tbCell.imageView startAnimating];
            
            //download the image asynchronously
            [self downloadImageWithURL:[NSURL URLWithString:flickrPhoto.photoURL] completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded)
                {
                    [tbCell.imageView stopAnimating];
                    // change the image in the cell
                    tbCell.imageView.image = image;
                    
                    // cache the image for use later (when scrolling back)
                    flickrPhoto.thumbnail = image;
                }
            }];
        }
        tbCell.backgroundColor = flickrPhoto.selected ? [UIColor redColor] : [UIColor clearColor];
        return tbCell;
    }
}

- (IBAction)btnRefreshPressed:(id)sender
{
    [self loadArrayFromFlickrPhotos];
}
@end
