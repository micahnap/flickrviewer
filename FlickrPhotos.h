//
//  FlickrPhotos.h
//  FlickChallenge
//
//  Created by Micah on 13/11/2014.
//  Copyright (c) 2014 Micah. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface FlickrPhotos : NSObject

@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, strong) NSString *photoURL;
@property bool selected;

-(id)initWithImageURL:(NSString *)photoURL andSelection:(BOOL)selected;
@end
