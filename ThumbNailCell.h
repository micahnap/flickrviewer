//
//  ThumbNailCell.h
//  FlickChallenge
//
//  Created by Micah on 13/11/2014.
//  Copyright (c) 2014 Micah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbNailCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end
