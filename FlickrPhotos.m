//
//  FlickrPhotos.m
//  FlickChallenge
//
//  Created by Micah on 13/11/2014.
//  Copyright (c) 2014 Micah. All rights reserved.
//

#import "FlickrPhotos.h"

@implementation FlickrPhotos

-(id)initWithImageURL:(NSString *)photoURL andSelection:(BOOL)selected
{
    if (self)
    {
        self.selected = selected;
        self.photoURL = photoURL;
    }
    
    return self;
}
@end
